terraform {
  required_providers {
    local = {
      source  = "hashicorp/local"
      version = "2.5.2"
    }
  }
}

resource "local_file" "expert" {
  filename = "./ignored-by-gitignore.txt"
  content  = "This text is ignored by gitignore."
}
