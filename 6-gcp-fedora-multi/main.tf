terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "6.2.0"
    }
  }
}

variable "gcp_project" { default = "understanding-terraform" }
variable "gcp_region" { default = "europe-west1" }
variable "gcp_zone" { default = "europe-west1-b" }
variable "hostname" { default = "vm" }
variable "machine_type" { default = "e2-medium" } # CPU 2 MEMORY 4096
variable "cloud_img" { default = "https://www.googleapis.com/compute/v1/projects/fedora-cloud/global/images/fedora-cloud-base-gcp-39-beta-1-1-x86-64" }
variable "vm_count" { default = 3 }

provider "google" {
  project = var.gcp_project
  region  = var.gcp_region
}

resource "google_compute_instance" "default" {
  count        = var.vm_count
  name         = "${var.hostname}${count.index + 1}"
  machine_type = var.machine_type
  zone         = var.gcp_zone

  boot_disk {
    initialize_params {
      image = var.cloud_img
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }
}

output "vm_ips" {
  value = google_compute_instance.default[*].network_interface[0].access_config[0].nat_ip
}
