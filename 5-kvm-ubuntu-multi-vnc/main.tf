terraform {
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.7.6"
    }
  }
}

provider "libvirt" {
  uri = "qemu:///system"
}

variable "hostname" { default = "vm" }
variable "domain" { default = "libvirt" }
variable "ip_type" { default = "dhcp" } # dhcp is other valid type
variable "memory" { default = 1024 * 4 }
variable "cpu" { default = 2 }
variable "username" { default = "thibault" }
variable "password" { default = "root" }
variable "cloud_img" { default = "../ubuntu24.img" }
variable "vm_count" { default = 5 }

resource "libvirt_volume" "os_image" {
  count = var.vm_count


  name   = "${var.hostname}${count.index + 1}-os_image"
  pool   = "default"
  source = var.cloud_img
  format = "qcow2"
}

resource "libvirt_cloudinit_disk" "commoninit" {
  count = var.vm_count

  name           = "${var.hostname}${count.index + 1}-commoninit.iso"
  pool           = "default"
  user_data      = data.template_cloudinit_config.config[count.index].rendered
  network_config = data.template_file.network_config.rendered
}

data "template_file" "user_data" {
  count = var.vm_count

  template = file("${path.module}/cloud_init.cfg")
  vars = {
    username   = var.username
    password   = var.password
    hostname   = "${var.hostname}${count.index + 1}"
    fqdn       = "${var.hostname}${count.index + 1}.${var.domain}"
    public_key = file("~/.ssh/id_ed25519.pub")
  }
}

data "template_cloudinit_config" "config" {
  count = var.vm_count

  gzip          = false
  base64_encode = false
  part {
    filename     = "init.cfg"
    content_type = "text/cloud-config"
    content      = data.template_file.user_data[count.index].rendered
  }
}

data "template_file" "network_config" {
  template = file("${path.module}/network_config_${var.ip_type}.cfg")
}

resource "libvirt_domain" "default" {
  count = var.vm_count

  name   = "${var.hostname}${count.index + 1}"
  memory = var.memory
  vcpu   = var.cpu

  disk {
    volume_id = libvirt_volume.os_image[count.index].id
  }
  network_interface {
    network_name = "default"
  }

  cloudinit = libvirt_cloudinit_disk.commoninit[count.index].id

  console {
    type        = "pty"
    target_port = "0"
    target_type = "serial"
  }

  graphics {
    type        = "vnc"
    listen_type = "address"
    autoport    = "true"
  }

  video {
    type = "qxl"
  }

  cpu {
    mode = "host-passthrough"
  }
}

output "ips" {
  value = libvirt_domain.default.*.network_interface.0.addresses
}
