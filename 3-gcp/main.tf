terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "6.2.0"
    }
  }
}

variable "gcp_project" { default = "understanding-terraform" }
variable "gcp_region" { default = "europe-west1" }
variable "gcp_zone" { default = "europe-west1-b" }
variable "vm_name" { default = "vm-debian-12" }
variable "vm_type" { default = "e2-micro" }
variable "cloud_img" { default = "debian-cloud/debian-12" }

provider "google" {
  # credentials = file("secret.json") # DevSecOps nightmare, avoid
  project = var.gcp_project
  region  = var.gcp_region
}

resource "google_compute_instance" "vm_instance" {
  name         = var.vm_name
  machine_type = var.vm_type
  zone         = var.gcp_zone

  boot_disk {
    initialize_params {
      image = var.cloud_img
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }
}

