output "vm_ips" {
  description = "The external IPs of the created VMs"
  value       = google_compute_instance.default[*].network_interface[0].access_config[0].nat_ip
}
