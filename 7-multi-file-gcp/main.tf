resource "google_compute_instance" "default" {
  count        = length(var.hostname_list)
  name         = var.hostname_list[count.index]
  machine_type = var.machine_type
  zone         = var.gcp_zone

  boot_disk {
    initialize_params {
      image = var.cloud_img
    }
  }

  network_interface {
    network = "default"
    access_config {}
  }
}
