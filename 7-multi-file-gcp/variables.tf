variable "gcp_project" {
  description = "The Google Cloud project ID"
  default     = "understanding-terraform"
}

variable "gcp_region" {
  description = "The Google Cloud region"
  default     = "europe-west1"
}

variable "gcp_zone" {
  description = "The Google Cloud zone"
  default     = "europe-west1-b"
}

variable "hostname_list" {
  description = "List of hostnames for the VMs"
  type        = list(string)
  default     = ["loadbalancer", "controlplane", "agent"]
}

variable "machine_type" {
  description = "Type of machine to be created"
  default     = "e2-medium"
}

variable "cloud_img" {
  description = "Cloud image for the VM"
  default     = "https://www.googleapis.com/compute/v1/projects/fedora-cloud/global/images/fedora-cloud-base-gcp-39-beta-1-1-x86-64"
}
