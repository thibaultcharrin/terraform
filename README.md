# Terraform

This project aims to provide terraform examples and use-cases.

## Prerequisites

- Terraform CLI
- Graphviz
- Qemu/KVM (libvirt)
- gcloud CLI
- (optional) cockpit and cockpit-machines

## Getting started

Prior to following each step of this guide, you may execute the following script:

```bash
./get-cloud-images.sh
```

### Pro Tips

```bash
alias t='terraform'
t init
t validate
t plan -out tfplan
t show tfplan
t graph | dot -Tsvg >graph.svg
t apply tfplan
t show
# each command can have the flag -json to become machine-readable
# for example:
t show tfplan -json | jq
```

## 1. Local - managing a file

**NB**: native Hashicorp providers do not need to be specified in **main.tf**, however it is best practice to fix the provider's version.

![local](./1-local/graph.svg)

## 2. Kubernetes - managing resources

Be sure to have a Kubernetes cluster running with the config file in **~/.kube/config**.

![kubernetes](./2-kubernetes/graph.svg)

## 3. Google Cloud Platform - managing a GCE instance

**NB**: Do not forget to destroy resources.

Be sure to install **gcloud cli**, create a project in **GCP** (if not existant), login using the **gcloud init** and **gcloud auth application-default login** commands. 

![gcp](./3-gcp/graph.svg)

## 4. KVM Ubuntu - managing a virtual machine

You may attempt to use **DHCP** by adding **nameserver 192.168.122.1** to **/etc/resolv.conf**. 

A reverse lookup should resolve your hostname:

```bash
nslookup <IP>   # displays hostname
# you may need to set 'libvirt' as the domain
# WSL2 might have issues in resolving corretly
```

![kvm](./4-kvm-ubuntu/graph.svg)

## 5. KVM Ubuntu Multi VNC - managing multiple virtual machines

You may observe the consoles of each virtual machine via **cockpit-machines** by navigating to **127.0.0.1:9090**

![kvm](./5-kvm-ubuntu-multi-vnc/graph.svg)

### 6. GCP Fedora Multi - lift and shift to the cloud

**NB**: Do not forget to destroy resources.

![kvm](./6-gcp-fedora-multi/graph.svg)

### 7. Multi File GCP - an introduction to modules

**NB**: Do not forget to destroy resources.


![kvm](./7-multi-file-gcp/graph.svg)