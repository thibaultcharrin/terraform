#!/bin/bash

# IMG_FEDORA='fedora40.qcow2'
IMG_UBUNTU='ubuntu24.img'
# URL_FEDORA='https://download.fedoraproject.org/pub/fedora/linux/releases/40/Cloud/x86_64/images/Fedora-Cloud-Base-Generic.x86_64-40-1.14.qcow2'
URL_UBUNTU='https://cloud-images.ubuntu.com/noble/current/noble-server-cloudimg-amd64.img'

function get_images() {
    if [ ! -f ./"${1}" ]; then
        if [ ! -f ~/"${1}" ]; then
            curl -Lo ~/"${1}" "${2}"
            cp -v ~/"${1}" ./"${1}"
        else
            cp -v ~/"${1}" ./"${1}"
        fi
    else
        echo "${1} is already present in $(pwd)"
    fi
}

get_images $IMG_UBUNTU $URL_UBUNTU
# get_images $IMG_FEDORA $URL_FEDORA
